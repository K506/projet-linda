package linda.shm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import linda.Callback;
import linda.Linda;
import linda.Tuple;

/** Shared memory implementation of Linda. */
public class ConcurrentCentralizedLinda implements Linda {
	
	private class CallbackStruct {
		public Callback callback;
		public eventMode mode;
		public Tuple template;
		
		public CallbackStruct(Callback callback, eventMode mode, Tuple template) {
			this.callback = callback;
			this.mode = mode;
			this.template = template;
		}
	}
	
	private class WriteThread implements Runnable {
		private ConcurrentCentralizedLinda linda;
		private Tuple t;
		
		public WriteThread(ConcurrentCentralizedLinda linda, Tuple t) {
			this.linda= linda;
			this.t = t;
		}
		
		public void run() {
			linda.lock.writeLock().lock();
			linda.tuples.add(t);
			linda.lock.writeLock().unlock();
			linda.notifyAll();
			linda.runCallbacks();	
		}
	}
	
	private ArrayList<Tuple> tuples;
	private ArrayList<CallbackStruct> callbacks;
	private ReentrantReadWriteLock lock;
	private ReentrantReadWriteLock callbacksLock;
	
    public ConcurrentCentralizedLinda() {
    	tuples = new ArrayList<>();
    	callbacks = new ArrayList<>();
    	lock = new ReentrantReadWriteLock();
    	callbacksLock = new ReentrantReadWriteLock();
    }

	@Override
	public synchronized void write(Tuple t) {
		new WriteThread(this, t).run();
	}

	private Tuple getTuple(Tuple template, boolean take, int from) {
		this.lock.readLock().lock();
		for (int i = from; i < this.tuples.size(); i++) {
			Tuple t = this.tuples.get(i);
			if (t.matches(template)) {
				if (take) {
					this.lock.readLock().unlock();
					this.lock.writeLock().lock();
					this.tuples.remove(i);
					this.lock.writeLock().unlock();
					this.lock.readLock().lock();
				}
				this.lock.readLock().unlock();
				return t;
			}
		}
		this.lock.readLock().unlock();
		return null;
	}
	
	private ArrayList<Tuple> getAll(Tuple template, boolean take) {
		ArrayList<Tuple> collection = new ArrayList<>();
		Tuple t;
		int i = 0;
		do {
			t = this.getTuple(template, take, i);
			if (t != null) {
				collection.add(t);
				i = this.tuples.indexOf(t) + 1;
			}
		} while(t != null);
		return collection;
	}
	
	private boolean tryCallback(CallbackStruct callback) {
		this.callbacksLock.readLock().lock();
		Tuple t = null;
		if (callback.mode == eventMode.READ) {
			t = this.tryRead(callback.template);
		} else if (callback.mode == eventMode.TAKE) {
			t = this.tryTake(callback.template);
		}
		if (t != null) {
			callback.callback.call(t);
			this.callbacksLock.readLock().unlock();
			return true;
		}
		this.callbacksLock.readLock().unlock();
		return false;
	}
	
	private void runCallbacks() {
		this.callbacksLock.readLock().lock();
		for (int i = 0; i < this.callbacks.size(); i++) {
			CallbackStruct c = this.callbacks.get(i);
			if (tryCallback(c)) {
				this.callbacksLock.readLock().unlock();
				this.callbacksLock.writeLock().lock();
				this.callbacks.remove(c);
				this.callbacksLock.writeLock().unlock();
				this.callbacksLock.readLock().lock();
			}
		}
		this.callbacksLock.readLock().unlock();
	}
	
	@Override
	public synchronized Tuple take(Tuple template) {
		Tuple t;
		t = this.tryTake(template);
		if (t != null) {
			return t;
		}
		do {
			try {
				this.wait();
				t = this.tryTake(template);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while(t == null);
		return t;
	}

	@Override
	public synchronized Tuple read(Tuple template) {
		Tuple t;
		t = this.tryRead(template);
		if (t != null) {
			return t;
		}
		do {
			try {
				this.wait();
				t = this.tryRead(template);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} while(t == null);
		return t;
	}

	@Override
	public Tuple tryTake(Tuple template) {
		return this.getTuple(template, true, 0);
	}

	@Override
	public Tuple tryRead(Tuple template) {
		return this.getTuple(template, false, 0);
	}

	@Override
	public Collection<Tuple> takeAll(Tuple template) {
		return this.getAll(template, true);
	}

	@Override
	public Collection<Tuple> readAll(Tuple template) {
		return this.getAll(template, false);
	}

	@Override
	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
		CallbackStruct cbs = new CallbackStruct(callback, mode, template);
		if (timing == eventTiming.IMMEDIATE) {
			if (!this.tryCallback(cbs)) {
				this.callbacksLock.writeLock().lock();
				this.callbacks.add(cbs);
				this.callbacksLock.writeLock().unlock();
			}
		} else if (timing == eventTiming.FUTURE) {
			this.callbacksLock.writeLock().lock();
			this.callbacks.add(cbs);
			this.callbacksLock.writeLock().unlock();
		}
	}

	@Override
	public void debug(String prefix) {
		System.out.println(prefix + "debug called");
	}
	
}
