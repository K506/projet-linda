package linda;

import java.rmi.RemoteException;

public class CallbackWrapper implements Callback {

	private RemoteCallbackInterface cb;

    public CallbackWrapper(RemoteCallbackInterface cb) {
    	this.cb = cb;
    }
    
    public void call(final Tuple t) {
    	try {
    		cb.call(t);
    	} catch (RemoteException e) {
			e.printStackTrace();
    	}
	}
}
