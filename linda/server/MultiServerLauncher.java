package linda.server;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class MultiServerLauncher {

	public static void main(String[] args) {
		try {
			new LindaMultiServer();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}
