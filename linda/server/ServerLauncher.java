package linda.server;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class ServerLauncher {

	public static void main(String[] args) {
		try {
            LocateRegistry.createRegistry(1099);
        } catch (java.rmi.server.ExportException e) {
            System.out.println("A registry is already running, proceeding...");
        } catch (RemoteException e) {
			e.printStackTrace();
		}

		LindaMonoServer lindaServer;
		try {
			lindaServer = new LindaMonoServer();
			java.rmi.Naming.rebind("rmi://localhost:1099/linda", lindaServer);

			System.out.println ("System is ready.");
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

}
