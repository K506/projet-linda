package linda.server;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Collection;

import linda.Callback;
import linda.Linda;
import linda.RemoteCallback;
import linda.Tuple;

/** Client part of a client/server implementation of Linda.
 * It implements the Linda interface and propagates everything to the server it is connected to.
 * */
public class LindaClientGeneric<ServerType extends LindaServer> implements Linda {
	
	protected ServerType lindaServer;
	private String uri;
	private boolean connected;
	
	/** Initializes the Linda implementation.
     *  @param serverURI the URI of the server, e.g. "rmi://localhost:4000/LindaServer" or "//localhost:4000/LindaServer".
     */
    public LindaClientGeneric(String serverURI) {
    	this.connected = false;
    	this.uri = serverURI;
    	try {
			this.lindaServer = (ServerType) Naming.lookup(serverURI);
			this.connected = true;
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			System.out.println("Unable to connect to server.");
		}
    }
    
    public String getURI() {
    	return this.uri;
    }

	public void write(Tuple t) {
		try {
			this.lindaServer.write(t);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	public Tuple take(Tuple template) {
		try {
			return this.lindaServer.take(template);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Tuple read(Tuple template) {
		try {
			return this.lindaServer.read(template);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Tuple tryTake(Tuple template) {
		try {
			return this.lindaServer.tryTake(template);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Tuple tryRead(Tuple template) {
		try {
			return this.lindaServer.tryRead(template);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Collection<Tuple> takeAll(Tuple template) {
		try {
			return this.lindaServer.takeAll(template);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Collection<Tuple> readAll(Tuple template) {
		try {
			return this.lindaServer.readAll(template);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void eventRegister(eventMode mode, eventTiming timing, Tuple template, Callback callback) {
		try {
			this.lindaServer.eventRegister(mode, timing, template, new RemoteCallback(callback));
		} catch (RemoteException e) {
			e.printStackTrace();
		} 
	}

	public void debug(String prefix) {
		try {
			this.lindaServer.debug(prefix);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + " - server @ " + this.uri + " - " + (this.connected ? "" : "not ") + "connected";
	}
}
