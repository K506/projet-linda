package linda.server;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import linda.Tuple;

public class LindaMultiServerClient extends LindaClientGeneric<LindaMultiServerInterface> implements Serializable {

	public LindaMultiServerClient(String serverURI) {
		super(serverURI);
	}

	public void wakeUp() {
		try {
			this.lindaServer.wakeUp();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	public void addClient(String clientURI) {
		try {
			this.lindaServer.addClient(clientURI);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
}
