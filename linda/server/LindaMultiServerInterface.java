package linda.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import linda.Tuple;
import linda.RemoteCallbackInterface;
import linda.Linda.eventMode;
import linda.Linda.eventTiming;

public interface LindaMultiServerInterface extends LindaServer, Remote {	
	public void addClient(String address) throws RemoteException;
	
	public int getNewPort() throws RemoteException;

	public ArrayList<String> getClients() throws RemoteException;
	
	public void wakeUp() throws RemoteException;
}
