package linda;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RemoteCallback extends UnicastRemoteObject implements RemoteCallbackInterface {

	private Callback cb;

    public RemoteCallback(Callback cb) throws RemoteException {
    	this.cb = cb;
    }
    
    public void call(final Tuple t) throws RemoteException {
    	cb.call(t);
	}
}
