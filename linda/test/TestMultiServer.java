package linda.test;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import linda.Linda;
import linda.Tuple;

public class TestMultiServer {

	//Linda server
	//You need to launch 3 server with MultiServerLauncher
	Linda linda1 = new linda.server.LindaClient("rmi://localhost:10000/linda");
	Linda linda2 = new linda.server.LindaClient("rmi://localhost:10001/linda");
	Linda linda = new linda.server.LindaClient("rmi://localhost:10002/linda");

	//motif 
	Tuple motifii = new Tuple(Integer.class, Integer.class);
	Tuple motifis = new Tuple(Integer.class, String.class);
	Tuple motifsi = new Tuple(String.class, Integer.class);
	
	//Tuple Test
	Tuple t1 = new Tuple(4, 5);
	Tuple t11 = new Tuple(6, 5);
	Tuple t2 = new Tuple("hello", 15);
	Tuple t3 = new Tuple(4, "foo");
	Tuple t42 = new Tuple(42, 42);

	//First Write Thread
	Thread tw = new Thread() {
		public void run() {
		    linda1.write(t1);
		    linda1.write(t11);
		    linda1.write(t2);
		    linda1.write(t3);
        }
	}; 

	//Thread to stop lock state
	Thread tw2 = new Thread() {
		public void run() {
			try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
			System.out.println("tw2 launch !");
		    linda2.write(t42);
        }
	};
	
	@Before
	public void setUp() throws Exception {
		try {
			tw.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() throws Exception {
		linda1.takeAll(motifii);
		linda1.takeAll(motifis);
		linda1.takeAll(motifsi);
		linda2.takeAll(t42);
	}

	@Test
	public void testNotExistTryTake() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Tuple tu = linda.tryTake(new Tuple("jawaj", "jawaj"));
				assertNull(tu);
				System.out.println("testNotExistTryTake finish");
			}
		};
		t.start();
		t.join();
	}

	@Test
	public void testBasicTryTake() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				linda.write(t2);
				Tuple ta = linda.tryTake(motifsi);
				assertEquals(t2, ta);
				assertNull(linda.tryTake(motifsi));
				System.out.println("testBasicTryTake finish");
			}
		};
		t.start();
		t.join();
	}

	@Test
	public void testBasicTakeAll() throws InterruptedException {
		Thread t = new Thread() {
			Collection<Tuple> colT;
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }				
				linda.write(t1);
				linda.write(t1);
				colT = linda.takeAll(motifii);
				assertEquals(2, colT.size());
				assertTrue(colT.contains(new Tuple(4, 5)));
				System.out.println("testBasicTakeAll finish");
			}
		};
		t.start();
		t.join();
	}
	
	@Test
	public void testBasicTryRead() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				linda.write(t3);
				assertEquals(t3, linda.tryRead(motifis));
				assertNotNull(linda.tryTake(motifis));
				System.out.println("testBasicTryRead finish");
			}
		};
		t.start();
		t.join();
	}

	@Test
	public void testNotExistTryRead() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Tuple tu = linda.tryRead(new Tuple("jawaj", "jawaj"));
				assertNull(tu);
				System.out.println("testNotExistTryRead finish");
			}
		};
		t.start();
		t.join();
	}

	@Test
	public void testBasicTake() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Tuple ta = linda.take(motifsi);
				assertEquals(t2, ta);
				assertNull(linda1.tryTake(motifsi));
				System.out.println("testBasicTake finish");
			}
		};
		t.start();
		t.join();
	}

	@Test
	public void testBasicNotExistTake() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Tuple ta = linda.take(t42);
				assertEquals(t42, ta);
				assertNull(linda2.tryTake(t42));
				System.out.println("testNotExistTake finish");
			}
		};
		tw2.start();
		t.start();
		t.join();
	}

	@Test
	public void testNotExistTakeAll() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Collection<Tuple> colT = linda.takeAll(t42);
				assertEquals(0, colT.size());
				assertFalse(colT.contains(t42));
				System.out.println("testNotExistTakeAll finish");
			}
		};
		t.start();
		t.join();
	}

	@Test
	public void testBasicRead() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Tuple ta = linda.read(motifis);
				assertEquals(t3, ta);
				assertNotNull(linda1.tryTake(motifis));
				System.out.println("testBasicRead finish");
			}
		};
		t.start();
		t.join();
	}

	@Test
	public void testNotExistRead() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Tuple ta = linda.read(t42);
				assertEquals(t42, ta);
				assertNotNull(linda2.tryRead(t42));
				System.out.println("testNotExistRead finish");
			}
		};
		tw2.start();
		t.start();
		t.join();
	}

	@Test
	public void testNotExistReadAll() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
				Collection<Tuple> colT = linda.readAll(t42);
				assertEquals(0, colT.size());
				assertFalse(colT.contains(t42));
				System.out.println("testNotExistReadAll finish");
			}
		};
		t.start();
		t.join();
	}

	/*
	@Test
	public void testBase() throws InterruptedException {
		Thread t = new Thread() {
			public void run() {
				try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
			}
		};
		t.start();
		t.join();
	}
	*/
}
